package co.bytemark.android.opentools.slider;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * Created by Ariel on 6/14/13.
 */
public class Slider extends SurfaceView implements SurfaceHolder.Callback {

	private static final String TAG = "Slider";
	private SurfaceHolder holder;
	private int canvasWidth;
	private Handler mHandler = new Handler();

	private OnSideListener listener;
	private boolean firedLeft = true;
	private boolean running;

	/*Slider Properties */
	private float slideX1 = 44;
	private float slideX2 = 194;
	private int sliderWidth = 150;
	private int sliderSpeed = 20;
	private int slideBorder = 3;
	private String sliderColor = "#FFF8F8F9";

	/*Auto-reset properties*/
	private int direction;
	public static final int DIRECTION_LEFT = 1;
	public static final int DIRECTION_RIGHT = -1;

	/*Layout properties*/
	private int border = 5;
	private int topColor = Color.parseColor("#FF1FC4F4");
	private int bottomColor = Color.parseColor("#FF00AEEF");
	private int borderColor = Color.parseColor("#FF007AC2");
	private String background = "#EBECED";

	public Slider(Context context) {
		super(context);
		holder = getHolder();
		holder.addCallback(this);
	}

	public Slider(Context context, AttributeSet attrs) {
		super(context, attrs);
		holder = getHolder();
		holder.addCallback(this);
		setZOrderOnTop(true);
	}

	public Slider(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		holder = getHolder();
		holder.addCallback(this);
		setZOrderOnTop(true);
	}

	public void surfaceCreated(SurfaceHolder holder) {
		Canvas canvas = holder.lockCanvas();
		canvas.drawColor(Color.WHITE);
		canvasWidth = canvas.getWidth();

		draw(canvas);
		holder.unlockCanvasAndPost(canvas);
		running = true;
	}

	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
	}

	public void surfaceDestroyed(SurfaceHolder holder) {
		running = false;
	}

	@Override
	public void draw(Canvas canvas) {
		canvas.drawARGB(255, 235, 236, 237);
		canvas.drawColor(Color.parseColor(background));

		Paint color = new Paint();

		color.setColor(borderColor);
		canvas.drawRoundRect(new RectF(0, 0, canvas.getWidth(), canvas.getHeight()), 10f, 10f, color);

		color.setColor(topColor);
		canvas.drawRoundRect(new RectF(border, border, canvas.getWidth() - border, canvas.getHeight() / 2), 10f, 10f, color);
		canvas.drawRect(border, 10, canvas.getWidth() - border, canvas.getHeight() / 2, color); //Draw Top Bar
		color.setColor(bottomColor);
		canvas.drawRoundRect(new RectF(border, canvas.getHeight() / 2, canvas.getWidth() - border, canvas.getHeight() - border), 10f, 10f, color);
		canvas.drawRect(border, canvas.getHeight() / 2, canvas.getWidth() - border, (canvas.getHeight() - 5) - 10, color); //Draw Bottom Bar
		color.setColor(Color.parseColor(sliderColor));
		canvas.drawRoundRect(new RectF(slideX1, 4, slideX2, canvas.getHeight() - 4), 10f, 10f, color); //Draw Slider
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) { //Return true to stop tracking event
		event.getAction();
		if (event.getAction() == MotionEvent.ACTION_UP) {
			if (slideX1 + 75 < canvasWidth / 2f) {
				mHandler.removeCallbacks(moveSlider);
				mHandler.postDelayed(moveSlider, 100);
				direction = Slider.DIRECTION_LEFT;
			} else {
				mHandler.removeCallbacks(moveSlider);
				mHandler.postDelayed(moveSlider, 100);
				direction = Slider.DIRECTION_RIGHT;
			}
			return false;
		} else {
			mHandler.removeCallbacks(moveSlider);
			if (slideX1 < event.getX() && slideX2 > event.getX()) {
				slideX1 = event.getX() - sliderWidth / 2f;
				slideX2 = event.getX() + sliderWidth / 2f;
			}
			Canvas canvas = getHolder().lockCanvas();

			if (canvas != null) {
				if (slideX2 > canvas.getWidth() - slideBorder) {
					slideX2 = canvas.getWidth() - slideBorder;
					slideX1 = slideX2 - sliderWidth;
				} else if (slideX1 < slideBorder) {
					slideX1 = slideBorder;
					slideX2 = slideX1 + sliderWidth;
				}
				draw(canvas);
				getHolder().unlockCanvasAndPost(canvas);
			}
			return true;
		}
	}

	public int getDirection() {
		return direction;
	}

	public int getSliderSpeed() {
		return sliderSpeed;
	}

	public void setSliderSpeed(int sliderSpeed) {
		this.sliderSpeed = sliderSpeed;
	}

	public void setBorder(int border) {
		this.border = border;
	}


	public void setSliderWidth(int sliderWidth) {
		this.sliderWidth = sliderWidth;
		slideX1 = slideBorder;
		slideX2 = slideBorder + this.sliderWidth;
	}

	private Runnable moveSlider = new Runnable() {
		@Override
		public void run() {
			if (running) {
				slideX1 -= direction * sliderSpeed;
				slideX2 -= direction * sliderSpeed;
				Canvas canvas = getHolder().lockCanvas();

				if (slideX2 > canvasWidth - slideBorder) {
					slideX2 = canvasWidth - slideBorder;
					slideX1 = slideX2 - sliderWidth;
					mHandler.removeCallbacks(moveSlider);
					if (listener != null & firedLeft) {
						firedLeft = false;
						listener.onSideMoved();
					}

				} else if (slideX1 < slideBorder) {
					slideX1 = slideBorder;
					slideX2 = slideX1 + sliderWidth;
					mHandler.removeCallbacks(moveSlider);
					if (listener != null && !firedLeft) {
						firedLeft = true;
						listener.onSideMoved();
					}
				}
				draw(canvas);

				getHolder().unlockCanvasAndPost(canvas);

				if (slideX1 != slideBorder || slideX2 != canvasWidth - slideBorder) {
					mHandler.postDelayed(this, 10);
				}
			}
		}
	};

	public void setDirection(int direction) {
		if(direction == DIRECTION_LEFT)
		{
			mHandler.removeCallbacks(moveSlider);
			mHandler.postDelayed(moveSlider, 100);
			this.direction = Slider.DIRECTION_LEFT;
		}else
		{
			mHandler.removeCallbacks(moveSlider);
			mHandler.postDelayed(moveSlider, 100);
			this.direction = DIRECTION_RIGHT;
		}
	}
	
	/**
	 * @deprecated use setTopColor(int topColor) instead
	 */
	public void setTopColor(String topColor)
	{
		this.topColor = Color.parseColor(topColor);
	}
	
	/**
	 * @deprecated use setBottomColor(int bottomColor) instead
	 */
	public void setBottomColor(String bottomColor)
	{
		this.bottomColor = Color.parseColor(bottomColor);
	}
	
	public void setTopColor(int topColor)
	{
		this.topColor = topColor;
	}
	
	public void setBottomColor(int bottomColor)
	{
		this.bottomColor = bottomColor;
	}
	
	public void setBorderColor(int borderColor)
	{
		this.borderColor = borderColor;
	}

	public interface OnSideListener {
		public void onSideMoved();
	}

	public void setOnSideListener(OnSideListener eventListener) {
		listener = eventListener;
	}
}