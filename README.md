Custom Android UI element I built while at Bytemark, fork was easier than transfering repo ownership, updates coming.

Must be placed in package co.bytemark.android.opentools.slider in order to work with Bytemark Inc. Opentools.
Part of the Bytemark Opentools Suite.

In order to use, add to XML like so:

	<co.bytemark.android.opentools.slider.Slider
        android:id="@+id/slider"
        android:layout_width="220dp"
        android:layout_height="75dp"/>
		
NOTE: Currently the slider is optimized for the dimensions 220dp x 75dp, update will ensure any size shortly. Slider properties can be hard coded into slider.java and soon to be made setters.
		
		
Implement OnSlideListener like so:

		Slider slider = (Slider) findViewById(R.id.slider);
				slider.setOnSideListener(new Slider.OnSideListener() {
				@Override
				public void onSideMoved() {
                
				}
        });
		
Use slider.getDirection() to compare with Slider.DIRECTION_LEFT and Slider.DIRECTION_RIGHT to find the slider's orientation.